from functools import wraps
from werkzeug.exceptions import BadRequest
from jsonschema import validate
from jsonschema.exceptions import ValidationError as JSONValidationError
from mongoengine.errors import ValidationError as MongoValidationError
from flask import request, jsonify
from app.definitions import MODEL_PATH

# All allowed extensions
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

# Check if filename is allowed
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Decorator to validate incoming JSON body (Flask)
def validate_json(f):
    @wraps(f)
    def wrapper(*args, **kw):
        try:
            request.json
        except BadRequest as e:
            msg = "Payload must be a valid json"
            return jsonify({"message": e.message, "status": 400}), 400
        return f(*args, **kw)
    return wrapper


# Decorator to validate incoming request body by JSON schema (Flask)
def validate_schema(schema):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                validate(request.json, schema)
            except JSONValidationError as e:
                return jsonify({"message": e.message, "status": 400}), 400
            return f(*args, **kw)
        return wrapper
    return decorator

# Decorator to get an item by id (Flask)
def get_by_id(model):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            try:
                item = model.objects().get(id=kw.get('id') or id)
            except MongoValidationError as e:
                return jsonify({ 'message': e.message, 'status': 404}), 404
            
            if not item:
                return jsonify({ 'message': 'Not found.', 'status': 404}), 404
                
            return f(item, *args, **kw)
        return wrapper
    return decorator
