import datetime
from bson import json_util
from mongoengine import *
from flask_mongoengine import BaseQuerySet
from app.review.model import HumanReview, RobotReview
from app.user.model import User

class CustomQuerySet(QuerySet):
    def to_json(self):
        return "[%s]" % (",".join([doc.to_json() for doc in self]))

class Asset(Document):
    created = DateTimeField(default=datetime.datetime.utcnow, required=True)
    author = ReferenceField(User)
    uri = StringField(default=None)
    location = GeoPointField(default=None)
    prediction = EmbeddedDocumentField(RobotReview, default=None)
    reviews = EmbeddedDocumentListField(HumanReview, default=None)

    meta = { 'queryset_class': CustomQuerySet }

    # Use to_json to add more info to json object, somehow mongoengine doesn't parse to usable json.
    def to_json(self):
        data = self.to_mongo()

        data['author'] = { '_id': self.author.id, 'username': self.author.username }

        return json_util.dumps(data)