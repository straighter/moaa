from flask import Blueprint, request, Response,abort, jsonify
from app import app, model
from app.asset.model import Asset
from app.review.model import HumanReview, RobotReview
from app.helpers import validate_json, validate_schema, allowed_file, get_by_id
from werkzeug.utils import secure_filename
from flask_jwt_extended import jwt_required
from bson import json_util
import os

from keras.models import load_model 
from keras.preprocessing.image import img_to_array
from PIL import Image
from app.definitions import MODEL_PATH
from keras.applications.inception_v3 import preprocess_input

import boto3, botocore
import numpy as np
import io

# Process the image to be usable by CNN
def process_image(img, target_size):
    if img.size != target_size:
        img = img.resize(target_size)

    x = img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    return x

# Do a binary prediction on the CNN
def predict_binary(img, model):
    prediction = model.predict(img)
    return prediction

prediction = Blueprint('prediction', __name__)
print(model)

# Predict an image's binary quality
@prediction.route('/predict', methods=['POST'])
@jwt_required
def predict():
    # Don't try if no model available
    if not model:
        return jsonify({'message': 'No model was loaded.', 'status': 400}), 400
    
    # Handle file upload
    file = request.files['file']

    if not file or file.filename is '':
        return jsonify({'message': 'No file.', 'status': 400}), 400
    
    if not allowed_file(file.filename):
        return jsonify({'message': 'File is not allowed.', 'status': 400}), 400

    image = file.read()
    image = Image.open(io.BytesIO(image))
    x = process_image(image, (299, 299))
    pred = predict_binary(x, model)
    return jsonify(value=float(pred[0][0]))
