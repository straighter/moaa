import os
import boto3, botocore
import plaidml.keras

# Install PlaidML backend over TF backend
plaidml.keras.install_backend()
from keras.models import load_model

import boto3, botocore
import os

from app.definitions import MODEL_PATH
#from app.helpers import download_model, set_model
from flask_mongoengine import MongoEngine
from flask import Flask
from flask_jwt_extended import JWTManager

env = os.environ.get('ENVIRONMENT', None)

app = Flask(__name__)

# Load right environment based on env var
if env == 'PRODUCTION':
    app.config.from_object('config.ProductionConfig')
elif env == 'DEVELOPMENT':
    app.config.from_object('config.DevelopmentConfig')
else:
    app.config.from_object('config.LocalConfig')

# Download model from S3 on startup and set it globally for later use
model = None

# Load the binary keras model and set global for use in other files
def set_model(save_dir):
    global model

    if os.path.exists(os.path.join(save_dir, 'binary_model.hd5')):
        model = load_model(os.path.join(save_dir, 'binary_model.hd5'))

# Download the binary keras model to the project
def download_model(s3_key, s3_secret, s3_bucket, s3_model_dir, save_dir):
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    else:
        return
    
    try:
        s3 = boto3.resource('s3', aws_access_key_id=s3_key, aws_secret_access_key=s3_secret)

        s3.Bucket(s3_bucket).download_file(s3_model_dir + 'binary_model.hd5', os.path.join(save_dir, 'binary_model.hd5'))

    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise
download_model(app.config.get('S3_KEY'), app.config.get('S3_SECRET'), app.config.get('S3_BUCKET'), app.config.get('S3_MODEL_FOLDER'), MODEL_PATH)
set_model(MODEL_PATH)

# Setup managers for global app use
jwt = JWTManager(app)
db = MongoEngine(app)

# Load endpoints using blueprints
from app.asset.views import asset
from app.user.views import user
from app.predict.views import prediction

app.register_blueprint(asset, url_prefix='/v1')
app.register_blueprint(user, url_prefix='/v1')
app.register_blueprint(prediction, url_prefix='/v1')

