import datetime
from mongoengine import *
from app.user.model import User

# Review by inspector
class HumanReview(EmbeddedDocument):
    author = ReferenceField(User)
    value = IntField(min_value = 0, max_value = 6)
    created = DateTimeField(default=datetime.datetime.utcnow, required=True)

# Prediction by ML
class RobotReview(EmbeddedDocument):
    value = DecimalField(min_value = 0, max_value = 1)


    
