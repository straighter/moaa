import json
from flask import Blueprint, request, abort, jsonify
from jsonschema import validate
from werkzeug.security import generate_password_hash
from flask_jwt_extended import jwt_required, create_access_token, create_refresh_token, get_jwt_identity, jwt_refresh_token_required
from app import app
from app.user.model import User
from bson import json_util
from app.helpers import validate_json, validate_schema, get_by_id
from mongoengine.errors import ValidationError as MongoValidationError


user = Blueprint('user', __name__)

# Define JSON schema's for user
user_creation_schema = {
    "type" : "object",
    "properties" : {
        "username" : {"type" : "string"},
        "password" : {"type" : "string"}

    }
}

user_login_schema = {
    "type" : "object",
    "properties" : {
        "username" : {"type" : "string"},
        "password" : {"type" : "string"}
    }
}

# Create a new user
@user.route('/users', methods=['POST'])
@validate_json
@validate_schema(user_creation_schema)
def create_user():
    userJSON = request.json

    if User.objects(username=userJSON.get('username')).first(): return jsonify(), 400

    user = User(username=userJSON.get('username'), password=generate_password_hash(userJSON.get('password')))
    user.save()
    return jsonify(), 201

# Log a user in
@user.route('/users/login', methods=['POST'])
@validate_json
@validate_schema(user_login_schema)
def login_user():
    userJSON = request.json
    user = User.objects(username=userJSON.get('username')).first()
    
    if not user: return jsonify(), 404
    if not user.validate_password(userJSON.get('password')): return jsonify(), 401
    
    id = str(user.id)
    access_token = create_access_token(identity=id)
    refresh_token = create_refresh_token(identity=id)
    
    return jsonify(access_token=access_token, refresh_token=refresh_token, username=userJSON.get('username'), user_id=id), 200

# Refresh access token for user
@user.route('/users/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    try:
        user = User.objects().get(id=get_jwt_identity())
    except MongoValidationError as e:
        return jsonify({ 'message': e.message, 'status': 404}), 404
    
    if not user:
        return jsonify({ 'message': 'Not found.', 'status': 404}), 404
        
    # Get user identity
    id = get_jwt_identity()
    username = user.username 
    access_token = create_access_token(identity=id)
    return jsonify(access_token=access_token, username=username, user_id=id), 200
