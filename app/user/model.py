import datetime
from mongoengine import *
from werkzeug.security import generate_password_hash, check_password_hash

class User(Document):
    username = StringField(required=True)
    password = StringField(required=True)
    created = DateTimeField(default=datetime.datetime.utcnow, required=True)
    meta = {'allow_inheritance': True}

    def validate_password(self, password):
        return check_password_hash(self.password, password)