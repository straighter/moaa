class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    JWT_SECRET_KEY = ''
    SECRET_KEY = ''
    S3_KEY = ''
    S3_SECRET = ''
    S3_BUCKET = ''
    S3_LOC = ''

class ProductionConfig(Config):
    DEBUG = False
    TESTING = False
    S3_IMAGE_FOLDER = 'jori/moaa/images_prod/'
    S3_MODEL_FOLDER = 'jori/moaa/models_prod'

    MONGODB_SETTINGS = {
        'db': 'moaa_prod',
        'host': '',
        'alias': 'default'
    }

class DevelopmentConfig(Config):
    DEBUG = True
    TESTING = True
    S3_IMAGE_FOLDER = 'jori/moaa/images_dev/'
    S3_MODEL_FOLDER = 'jori/moaa/models_dev'

    MONGODB_SETTINGS = {
        'db': 'moaa_dev',
        'host': '',
        'alias': 'default'
    }

class LocalConfig(Config):
    DEBUG = True
    TESTING = True
    S3_IMAGE_FOLDER = 'jori/moaa/images_local/'
    S3_MODEL_FOLDER = 'jori/moaa/models_local'

    MONGODB_SETTINGS = {
        'db': 'moaa_local',
        'host': 'mongodb://localhost:27017/moaa_local',
        'alias': 'default'
    }
